﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

[assembly: InternalsVisibleTo("05_ExceptionsTests")]
namespace Ch05
{
    public enum Country
    {
        UK, US
    }

    /// <summary>
    /// An address has a line, city, postal code and country.  
    /// Once an address is created, none of these attributes can be changed.  
    /// A country can only be from a fixed set of country codes.  
    /// A postal code must be of the format: 
    ///   US: 5 digits
    ///   UK: like one of these: N1 4EJ, WC2 1BQ, P10 9KV, NW11 8XY.  
    /// It must not be possible to create an address which does not conform to these rules.
    /// 
    /// </summary>
    
    public class Address
    {
        const string USFORMAT = "^[0-9]{5}$";
        const string UKFORMAT = @"^[A-Z]{1,2}[0-9]{1,2}\s*([0-9][A-Z]{2})?$";
        static Regex usRegex = new Regex(USFORMAT);
        static Regex ukRegex = new Regex(UKFORMAT);

        internal readonly string line1;
        internal readonly string city;
        internal readonly string postalCode;
        internal readonly Country code;

        public Address(string line1, string city, string postalCode, Country code)
        {
            ValidatePostalCode(postalCode, code);
            this.line1 = line1;
            this.city = city;
            this.postalCode = postalCode;
            this.code = code;
        }

        internal static void ValidatePostalCode(string postalCode, Country code)
        {
            switch (code)
            {
                case Country.US:
                     if (!usRegex.IsMatch(postalCode))
                        throw new ArgumentException("Bad zip code: " + postalCode);
                    break;
                case Country.UK:
                    if (!ukRegex.IsMatch(postalCode))
                        throw new ArgumentException("Bad post code: " + postalCode);
                    break;
                default:
                    break;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", line1, city, postalCode);
        }
    }
}

