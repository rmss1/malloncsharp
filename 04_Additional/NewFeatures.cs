﻿using System;
using System.Collections.Generic;

namespace MyExtensions
{
    public static class NewFeatures
    {
        public static IEnumerable<string> ToWords(this string source)
        {
            return source.Split(
            new[] { '.', ' ', ':', ';', ',', '?', '!' },
            StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
