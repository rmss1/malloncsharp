﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch04
{
    struct BinaryNumber
    {
        private int _val;
        private const int _base = 2;

        public BinaryNumber(String val)
        {
            _val = Convert.ToInt32(val, _base);
        }

        public static implicit operator int(BinaryNumber val)
        { return val._val; }

        public static explicit operator BinaryNumber(int val)
        {
            BinaryNumber tmp = new BinaryNumber();
            tmp._val = val;
            return tmp;
        }

        public static explicit operator BinaryNumber(String val)
        { return new BinaryNumber(val); }

        public override string ToString()
        {
            return Convert.ToString(_val, _base);
        }

    }
}
