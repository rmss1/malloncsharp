﻿using System;
using MyExtensions;

namespace Ch04
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryNumber a = new BinaryNumber("101");
            Console.WriteLine(a);

            a = (BinaryNumber)"1010101010";
            Console.WriteLine(a);

            a = (BinaryNumber)10;
            Console.WriteLine(a);

            int b = 5 + a;
            Console.WriteLine(b);
            Console.WriteLine();

            var results0 = NewFeatures.ToWords("a b c d e");
            foreach (var result in results0)
            {
                Console.WriteLine(result);
            }
            Console.WriteLine();

            string data = @"The C# 3.0 language and compiler introduce...";
            var results1 = data.ToWords();
            foreach (var result in results1)
            {
                Console.WriteLine(result);
            }

        }
    }
}
