﻿using System;

namespace Ch08
{
    public class AccountEventArgs : EventArgs
    {
        private double _balance;

        public AccountEventArgs(double balance)
        {
            _balance = balance;
        }

        public double Balance
        {
            get { return _balance; }
        }
    }
}
