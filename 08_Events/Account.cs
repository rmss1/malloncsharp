﻿using System;

namespace Ch08
{
    public class Account
    {
        public event EventHandler Overdrawn0;
        public event EventHandler<EventArgs> Overdrawn1;
        public event EventHandler<AccountEventArgs> Overdrawn2;

        private double _balance;
        private string _name;

        public Account(string name, double balance)
        {
            _name = name;
            _balance = balance;
        }

        public double Balance
        {
            get { return _balance; }
        }

        public string Name
        {
            get { return _name; }
        }

        public void Withdraw(double amt)
        {
            _balance -= amt;
            if (_balance < 0)
            {
                OnOverdrawn(new AccountEventArgs(_balance));
            }
        }

        protected virtual void OnOverdrawn(AccountEventArgs e)
        {
            // We invoke the target methods (whatever they might be)

            Overdrawn0(this, e);

            if (Overdrawn1 != null)
            {
                Overdrawn1(this, e);
            }

            EventHandler<AccountEventArgs> handler = Overdrawn2;
            if (handler != null)
            {
                handler(this, e);
            }  
        }

    }
}
