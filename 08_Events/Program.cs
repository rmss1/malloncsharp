﻿using System;

namespace Ch08
{
    class Program
    {
        static void Main(string[] args)
        {
            Account acc = new Account("Gerald Lewis", 10000);
            acc.Overdrawn0 += EmailOverdrawnHandler;
            acc.Overdrawn1 += EmailOverdrawnHandler;

            acc.Overdrawn2 += ConsoleOverdrawnHandler;
            acc.Overdrawn2 += DBOverdrawnHandler;

            acc.Withdraw(9000);
            acc.Withdraw(9000);
        }

        public static void EmailOverdrawnHandler(object sender, EventArgs e)
        {
            Account source = sender as Account;
            Console.WriteLine("Alert: {0} : {1}", source.Name, e.ToString());
        }

        static void ConsoleOverdrawnHandler(object sender, AccountEventArgs e)
        {
            Console.WriteLine("Overdrawn: Balance {0}, @ {1}", e.Balance, DateTime.Now);
        }

        public static void DBOverdrawnHandler(object sender, AccountEventArgs e)
        {
            Console.WriteLine("INSERT INTO overdrawn VALUES('{0}')", e.Balance);
        }


    }
}
