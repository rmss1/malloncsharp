﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ch05
{
    [TestClass]
    public class AddressTest
    {

        [TestMethod]
        public void AddressStringForm()
        {
            var acacia = new Address("12 Acacia Avenue", "London", "E10 7RJ", Country.UK);
            Assert.AreEqual(acacia.ToString(), "12 Acacia Avenue, London, E10 7RJ");
        }


        [TestMethod]
        public void UsAddressAcceptsAValidZipCode()
        {
            var usAddress =
                    new Address("1001 Main Street", "Springfield", "12345", Country.US);
            Assert.IsNotNull(usAddress);
        }

        [TestMethod]
        public void UkAddressAcceptsAValidPostcode()
        {
            var ukAddress =
                new Address("12 Acacia Avenue", "London", "E10 7RJ", Country.UK);
            Assert.IsNotNull(ukAddress);

            // Not quite right, because would error with NPE rather than fail:
            // Assert.IsInstanceOfType(ukAddress, typeof(Address));
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void UkAddressRejetsABadPostcode()
        {
            string tooManyChars = "E10 7RJX";
            new Address("12 Acacia Avenue", "London", tooManyChars, Country.UK);
        }

        [TestMethod]
        public void UkAddressRejetsBadPostcodeWithExplanatoryMessage()
        {
            string tooManyChars = "E10 7RJX";
            try
            {
                new Address("12 Acacia Avenue", "London", tooManyChars, Country.UK);
                Assert.Fail("An exception should have been thrown");
            }
            catch (Exception expected)
            {
                Assert.AreEqual(expected.Message, "Bad post code: " + tooManyChars);
            }
        }


        // delegate with ctor
        [TestMethod]
        public void UkAddressRejectsAnInValidPostcodeD()
        {
            string tooManyChars = "E10 7RJX";
            Assert.ThrowsException<ArgumentException>(delegate
            {
                new Address("12 Acacia Avenue", "London", tooManyChars, Country.UK);
            });
         }

        // delegate with method call; method is internal to application assembly
        [TestMethod]
        public void ValidatePostalCodeRejectsBadZipCodeD()
        {    
            Assert.ThrowsException<ArgumentException>(delegate
            {
                var tooManyDigits = "123456";
                Address.ValidatePostalCode(tooManyDigits, Country.US);
            });
        }

        // C# 3.0 ff - lambda syntax
        [TestMethod]
        public void UkAddressRejectsAnInValidPostcodeL()
        {
            string tooManyChars = "E10 7RJX";
            Assert.ThrowsException<ArgumentException>(
                () => new Address("12 Acacia Avenue", "London", tooManyChars, Country.UK)
            );
        }

        [TestMethod]
        public void ValidatePostalCodeRejectsBadZipCodeL()
        {
            var tooManyDigits = "123456";
            Assert.ThrowsException<ArgumentException>(() => Address.ValidatePostalCode(tooManyDigits, Country.US));
        }


        // Full versions
        [TestMethod]
        public void UkAddressRejectsAnInValidPostcode()
        {
            string tooManyChars = "E10 7RJX";
            ArgumentException expected = 
                Assert.ThrowsException<ArgumentException>(
                    () => new Address("12 Acacia Avenue", "London", tooManyChars, Country.UK)
                );
            Assert.AreEqual(expected.Message, "Bad post code: " + tooManyChars);
        }

        [TestMethod]
        public void ValidatePostalCodeRejectsBadZipCode()
        {
            var tooManyDigits = "123456";
            ArgumentException expected = 
                Assert.ThrowsException<ArgumentException>(() => Address.ValidatePostalCode(tooManyDigits, Country.US));
            Assert.AreEqual(expected.Message, "Bad zip code: " + tooManyDigits);
        }

     }

}

