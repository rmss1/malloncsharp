﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ch01
{
    [TestClass]
    public class PersonTest
    {
        Person person;

        [TestInitialize]
        public void SetUp()
        {
            person = new Person("John", "Smith", 23);
        }

        [TestMethod]
        public void NameIsFirstnameSpaceSurname()
        {
            Assert.AreEqual("John Smith", person.Name);
        }

        [TestMethod]
        public void ToStringIncludesAge()
        {
            Assert.AreEqual("John Smith [23]", person.ToString());
        }

        [TestMethod]
        public void HaveABirthDayIncrementsAge()
        {
            Assert.AreEqual(23, person.Age);    // guard assertion
            person.HaveBirthday();
            Assert.AreEqual(24, person.Age);
        }



    }
}
