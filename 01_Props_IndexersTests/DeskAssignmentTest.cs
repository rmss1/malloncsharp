﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ch01
{
    [TestClass]
    public class DeskAssignmentTest
    {
        DeskAssignment seating;

        [TestInitialize]
        public void Setup()
        {
            seating = new DeskAssignment();
            seating[2, "near door"] = new Person("Tom", "Smith", 21);
            seating[1, "by window"] = new Person("Dick", "Jones", 22);
            seating[1, "behind copier"] = new Person("Harry", "Hill", 23);
        }

        [TestMethod]
        public void CanLookUpByDesk()
        {
            Person found = seating["1:by window"];
            Assert.AreEqual("Dick Jones [22]", found.ToString());
        }

        [TestMethod]
        public void CanLookUpByFoorAndLocation()
        {
            Person found = seating[1, "behind copier"];
            Assert.AreEqual("Harry Hill [23]", found.ToString());
        }

    }
}
