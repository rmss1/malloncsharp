﻿using static System.Console;

namespace Ch01
{
    // Two C# 6 features illustrated 
    class Program
    {

        public static void Main()
        {
            Person fred = null;
            WriteLine(">" + fred?.ToString());  // evaluates to null if LHS is null

            SimpleDeskAssignment seating = new SimpleDeskAssignment();
            WriteLine("Count: {0}", seating.Count);
            WriteLine("Capacity: {0}", seating.Capacity);

            seating[0] = new Person("Tom", "Smith", 20);
            seating[4] = new Person("Dick", "Jones", 21);
            seating[1] = new Person("Harry", "Robinson", 22);

            for (int i = 0; i < seating.Count; ++i)
            {
                if (seating[i] != null)
                    WriteLine(seating[i].Name);
            }
            WriteLine("Count: {0}", seating.Count);
            WriteLine("Capacity: {0}", seating.Capacity);
        }
    }
}
