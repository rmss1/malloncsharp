﻿namespace Ch01
{
    public class Person
    {
        string forename;        // don't need to declare private
        string surname;

        public string Name {
            get
            {
                return forename + " " + surname;
            }
        }

        public int Age { get; private set; }
        public string Password { private get; set; } // writeable not readable

        public Person(string forename, string surname, int age)
        {
            this.forename = forename;
            this.surname = surname;
            Age = age;
        }

        public void HaveBirthday()
        {
            Age += 1;
        }

        public override string ToString()
        {
            return Name + " [" + Age + "]";
        }

    }
}
