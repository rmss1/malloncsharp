﻿using System.Collections;

namespace Ch01
{
    class SimpleDeskAssignment
    {
        ArrayList desks = new ArrayList();

        public Person this[int number]
        {
            get { return (Person)desks[number]; }
            set
            {
                // can assign into desk position beyond current count
                while (desks.Count <= number) desks.Add(null);
                desks[number] = value;
            }
        }

        public int Count
        {
            get
            {
                return desks.Count;
            }
        }

        public int Capacity
        {
            get
            {
                return desks.Capacity;
            }
        }
    }
}
