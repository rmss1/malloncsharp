﻿using System.Collections;

namespace Ch01
{
    public class DeskAssignment : IEnumerable
    {
        SortedList desks = new SortedList();

        public Person this[int floor, string position]
        {
            get
            {
                return (Person)desks[floor + ":" + position];
            }
            set { desks[floor + ":" + position] = value; }
        }

        public Person this[string desk]
        {
            get { return (Person)desks[desk]; }
        }

        public IEnumerator GetEnumerator()
        {
            return desks.Keys.GetEnumerator();
        }

        public int Count
        {
            get
            {
                return desks.Count;
            }
        }

        public int Capacity
        {
            get
            {
                return desks.Capacity;
            }
        }

    }
}
