﻿namespace Ch02
{

    class Derived : Base
    {
        // No ctor - illustrates the implicit default ctor implicitly calls Base ctor

        public override int Foo1()
        {
            return 101;
        }

        public override int Foo2()
        {
            return 102;
        }

        public new int Foo3()
        {
            return 103;
        }

        public override sealed int Foo4()
        {
            return 104;
        }

        //public override int Foo5()
        //{
        //    return 105;
        //}

        // Compiler warning: Derived.Foo6() hides inherited member Base.Foo6() ...
        public int Foo6()
        {
            return 106;
        }
    }

}
