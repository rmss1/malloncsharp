﻿using System;

namespace Ch02
{
    abstract class Base
    {
        // abstract class can still have a ctor (you just wouldn't be able to invoke it directly)
        public Base()
        {
            Console.WriteLine("Base ctor being called");
        }

        public abstract int Foo1();

        public virtual int Foo2()
        {
            return 2;
        }

        public virtual int Foo3()
        {
            return 3;
        }

        public virtual int Foo4()
        {
            return 4;
        }

        /*
        The implementation of a non-virtual method is invariant: 
        The implementation is the same whether the method is invoked on an instance of 
        the class in which it is declared or an instance of a derived class.
        https://msdn.microsoft.com/en-us/library/aa645767(v=vs.71).aspx
        */
        public int Foo5()
        {
            return 5;
        }

        public int Foo6()
        {
            return 6;
        }


    }
}
