﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using static System.Console;

namespace Ch02
{
    class Program
    {
        static void Main(string[] args)
        {

            Base obj = new Derived();

            WriteLine(obj.Foo1());
            WriteLine(obj.Foo2());
            WriteLine(obj.Foo3());
            WriteLine(obj.Foo4());

            WriteLine(obj.Foo6());

            WriteLine();


            // @ prefix strings
            // StreamReader sr = new StreamReader(@"..\..\Jobs.txt");
            string regex = @"(\d+)\s+(\S+|\s{10})\s+(\S+)\s+(\S+)\s+(\S+ \S+)\s+(.+)";
            Regex re = new Regex(regex);

            // C£ 6 string interpolation
            DateTime birthday = new DateTime(1934, 1, 12);
            Console.WriteLine($"Birthday: {birthday:D}");


        }
    }
}
