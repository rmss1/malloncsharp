﻿namespace Ch02
{
    class Person
    {

        // static ctor
        // no visibility modifier
        static Person()
        {
                
        }

        // compiler prevents (unlike Java)
        //public int Person()
        //{
        //    return 0;
        //}
    }
}
