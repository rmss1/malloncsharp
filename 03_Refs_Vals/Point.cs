﻿using System;

namespace Ch03
{
    struct Point : IChange
    {
        private int _x;
        private int _y;

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }
        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }
        public Point(int x_, int y_)
        {
            _x = x_;
            _y = y_;
        }

        public void Change(int val)
        {
            _x = _y = val;
        }
        public override string ToString()
        {
            return
            String.Format("{0},{1}", _x, _y);
        }
    }
}
