﻿using System;

namespace Ch03
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p = new Point(1, 2);
            object o = p;
            ((Point)o).Change(5);           // 1,2
            Console.WriteLine(o);
            ((IChange)o).Change(6);         // 6,6
            Console.WriteLine(o);
        }
    }
}
