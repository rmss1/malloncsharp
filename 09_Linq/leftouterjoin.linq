<Query Kind="Statements" />

List<char> vowels = new List<char>() { 'a', 'e', 'i', 'o', 'u'};

var ljq = from c in vowels
		join p in Process.GetProcesses() 
		on c equals p.ProcessName[0] into jg
		orderby c
		select new
		{
			Prefix = c,
			Processes = jg
		};