<Query Kind="Statements" />

var procs = from p in Process.GetProcesses()
		group p by p.Threads.Count into g
		orderby g.Key descending
		select g;

foreach (var group in procs)
{
	Console.WriteLine("{0} Threads:", group.Key);
	foreach (var proc in group)
	{
		Console.WriteLine("\t{0}", proc.ProcessName);
	}
}