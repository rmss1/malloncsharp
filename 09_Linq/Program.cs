﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ch09
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 5, 99, -8, 22, 0, 34, 7, 58 };

            IList<string> england = new List<string>
            { "Cook", "Hales", "Compton", "Root", "Taylor",
                "Stokes", "Bairstow", "Moeen", "Woakes",
                "Broad", "Anderson" };

            //SimpleNumbers(numbers);

            //SimpleStrings(england);

            //Aggregates(england);

            //Fluent(england);

            //Grouping1(numbers);

            //Grouping2(numbers);

            InnerJoinDemo();

            LeftOuterJoinDemo();

        }


        private static void SimpleNumbers(int[] numbers)
        {
            var query0 = from number in numbers
                         where number > 10
                         select number;
            Console.WriteLine(query0.Count());
            Console.WriteLine();
        }

        private static void SimpleStrings(IList<string> england)
        {
            var query = from player in england
                        where player.Length > 5
                        orderby player ascending
                        select player;

            foreach (string item in query)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

        private static void Aggregates(IList<string> england)
        {
            Console.WriteLine("Ave: " 
                + (int)(from p in england select p.Length).Average());

            var nested = from player in england
                         where player.Length <
                         (from p in england select p.Length).Average()
                         select player;

            foreach (string item in nested)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

        private static void Fluent(IList<string> england)
        {
            // don't need the Select
            var fluent = england
                         .Where(p => p.Length > 5)
                         .OrderBy(p => p);


            var fluentnested = england
                        .Where(p => p.Length <
                            (england.Select(p1 => p1.Length).Average()))
                             .OrderBy(p => p)
                             .Select(p => p);

            foreach (string item in fluentnested)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

        private static void Grouping1(int[] numbers)
        {
            // group numbers by remainder when divided by 2
            // just the groups: 
            var query3 = from n in numbers
                         group n by n % 2 into g
                         select g;
            foreach (var group in query3)
            {
                Console.Write("Group key: {0}. Members: ", group.Key);
     
                // To illustrate syntax (All needs function which returns, not void like Write() )
                group.All(x => { Console.Write(x + " "); return true; });
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        private static void Grouping2(int[] numbers)
        {
            var query2 = from n in numbers
                         group n by n % 2 into g
                         select new
                         {
                             Key = g.Key,
                             Type = g.GetType().ToString(),
                             Numbers = g
                         };
            foreach (var g in query2)
            {
                Console.WriteLine("{0} -- {1}", g.Key, g.Type);
                foreach (var n in g.Numbers)
                {
                    Console.WriteLine(n);
                }
            }
        }

        private static void InnerJoinDemo()
        {
            string[] left = "The quick brown fox jumps over the lazy dog".Split();
            string[] right = "The quick brown cow jumps over the lazy frog".Split();

            var query = from w0 in left
                        join w1 in right on w0 equals w1
                        select w1;

            foreach (string item in query)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

        private static void LeftOuterJoinDemo()
        {
            string[] left = "The quick brown fox jumps over the lazy dog".Split();
            string[] right = "The quick brown cow jumps over the lazy frog".Split();

            var query = from w0 in left
                        join w1 in right on w0 equals w1
                        into leftouter
                        select w0;
            // can't change w0 to w1 to make it right outer - not in scope
            // so easiest is to switch order of left and right

            foreach (string item in query)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

    }
}
