﻿using System;

namespace Ch07
{
    // delegate declared within class
    // used with 2 static methods
    // Math.Pow also matches its signature
    // method ref return value from method
    // can pass method to another method
    // multicast
    class Program
    {
        delegate double MathOp(double a, double b);

        private static double Sum(double a, double b)
        {
            return a + b;
        }

        private static double Product(double a, double b)
        {
            return a * b;
        }

        private static MathOp Intro()
        {
            double one = 4.5;
            double two = 8.0;

            MathOp func = new MathOp(Sum);
            // or just 
            func = Program.Sum;
            Console.WriteLine("{0}: {1}", func.Method.Name, func(one, two));

            // explicit use of Invoke: 
            func = new MathOp(Product);
            // or just 
            func = Product;
            Console.WriteLine("{0}: {1}", func.Method.Name, func.Invoke(one, two));

            func = new MathOp(Math.Pow);
            // or just 
            func = Math.Pow;
            Console.WriteLine("{0}: {1}", func.Method.Name, func(one, two));
            return func;
        }

        private static void DoSomething(MathOp func)
        {
            double alpha = 10.0;
            double beta = 2.0;
            Console.WriteLine("{0}: {1}", func.Method.Name, func(alpha, beta));
        }

        static void Main(string[] args)
        {
            MathOp func = Intro();
            DoSomething(func);
            Console.WriteLine();

            Multicast();
        }

        private static void Multicast()
        {
            LogInvoker logger = new LogInvoker(Dispatcher.SendEmail);
            logger += Dispatcher.LogToDB;
            logger += new LogInvoker(Dispatcher.LogToConsole);

            logger("how now brown cow");
            Console.WriteLine();

            logger -= Dispatcher.LogToDB;
            logger("the quick brown fox");

        }

    }
}
