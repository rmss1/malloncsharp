﻿using System;

namespace Ch07
{
    delegate void LogInvoker(string msg);

    class Dispatcher
    {
        public static void LogToDB(string msg)
        {
            Console.WriteLine("INSERT INTO messages VALUES('{0}')", msg);
        }

        public static void LogToConsole(string msg)
        {
            Console.WriteLine("{1} - {0}", msg, DateTime.Now);
        }

        public static void SendEmail(string msg)
        {
            Console.WriteLine("Sending your message: {0} ...", msg);
        }

    }
}
