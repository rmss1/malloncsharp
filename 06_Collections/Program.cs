using System;
using System.Collections;
using System.Collections.Generic;

namespace Ch06
{
    class Program
    {
        static void Main(string[] args)
        {
            IList people = new ArrayList();
            Console.WriteLine("people Count: " + people.Count);

            people.Add(new Person("Tom"));
            people.Add(new Person("Dick"));
            people.Add(new Person("Harry"));

            Console.WriteLine("people Count: " + people.Count);
            // people[3] = new Person("does not grow");
            people.Add(new Person("Mary"));

            for (int i = 0; i < people.Count; ++i)
                Console.WriteLine(((Person)people[i]).Name);

            foreach (Person p in people)
                Console.WriteLine(p.Name);

            people[0] = new Person("Francis");



            IList<Person> testData =
                new List<Person>()
                {
                    new Person("Doris"), new Person("Horace"), new Person("Boris")
                };

            for (IEnumerator<Person> e = testData.GetEnumerator(); e.MoveNext();)
                Console.WriteLine(e.Current.Name);

        }
    }
}
