﻿namespace Ch06
{
    // struct will be auto-boxed
    struct Person
    {
        public string Name { get; internal set; }

        public Person(string name)
        {
            Name = name;
        }

    }
}